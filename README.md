Just Anoter Remote Desktop Environment
======================

Jarde is a on-going project to make a "desktop environment" accessible from the browser to ease access, monitoring and interacting with linux servers remotely. It's being developed with React, React Redux and React Router with backend written in Node.js using Express.

## Features:

- File Manager (40% done)
- Terminal (70% done)
- Task Manager (TODO)
- Bookmarks (TODO)
- Workspaces (TODO)
- Multiple layouts and themes (TODO)
- Settings/Preferences page (TODO)

[![demo](/res/demo.gif)](/res/demo.gif)
