FROM nikolaik/python-nodejs:latest

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

COPY package.json ./
COPY yarn.lock ./
RUN yarn install

EXPOSE 8080
# start app
CMD ["yarn", "start"]
