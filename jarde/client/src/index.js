/* @format */
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';

//import "css/index.css";
//import "bootstrap/dist/css/bootstrap.min.css";
//import "css/sb-admin-2.css";
//import "css/animations.css";
//import "@fortawesome/fontawesome-free/css/all.css";

import App from './app';

import { Provider } from 'react-redux';
import store from 'common/store';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <App />
    </Router>
  </Provider>,
  document.getElementById('root')
);
