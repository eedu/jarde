/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';
import classNames from 'classnames';

class JardeLayout extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {page, navbar, dock, application, appState, className} = this.props;

    return (
      <div className={classNames('page-layout jarde', page, className)}>
        <div className="header">{navbar}</div>
        <div className="left-panel">{dock}</div>
        <div
          className={classNames('center-panel application', {
            'opening-app': appState === 'opening',
          })}>
          {application}
        </div>
        <div className="right-panel"></div>
        <div className="footer"></div>
      </div>
    );
  }
}

const mapStateToProps = state => {};

const mapActionsToProps = {};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(JardeLayout);
