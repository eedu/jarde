/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';

import {API_STATUS} from 'common/utils';
import execApi from 'common/api/exec';

import classNames from 'classnames';
import {merge, MergeParam, MERGE_ACTIONS} from 'common/functions/merge';

import FileManagerView from './view';

export const FileTypes = {
  FILE: 'file',
  FOLDER: 'folder',
  LINK: 'link',
};

class FileManager extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      explorer: {},
      files: [],
    };
  }

  componentDidMount() {
    this.updateExplorer();
    this.updateFiles();
  }

  getFileType(permissions) {}

  updateFiles() {
    const {path} = this.props;

    const self = this;

    execApi.ls(path).then(data => {
      if (data.status === API_STATUS.SUCCESS) {
        const files = data.output
          .trim()
          .split('\n')
          .map(file => {
            const [
              name,
              type,
              permissions,
              owner,
              group,
              size,
              time,
            ] = file.split(' // ');
            return {
              name: name.split(' -> ')[0],
              hidden: name.startsWith('.'),
              type,
              permissions: {
                read: permissions.indexOf('r') >= 0,
                write: permissions.indexOf('w') >= 0,
                execute: permissions.indexOf('x') >= 0,
              },
              owner,
              group,
              size,
              time,
            };
          });

        self.setState({
          files,
        });
      }
    });
  }

  updateExplorer() {
    const {path} = this.props;
    var acc = '';
    var folders = [];
    path.split('/').forEach(folder => {
      acc += (acc.endsWith('/') ? '' : '/') + folder;
      folders.push(acc);
    });

    const self = this;

    execApi.listDirectories(folders).then(data => {
      if (data.status === API_STATUS.SUCCESS) {
        const explorer = Object.keys(data.output)
          .sort((a, b) => b.length - a.length)
          .reduce((obj, key) => {
            const dirname = key.match(/[^\/]*$/)[0] || '/';

            const ls = data.output[key]
              .trim()
              .split('\n')
              .reduce((obj2, dirname) => {
                return merge(obj2, {[dirname]: null});
              }, {});

            if (!obj) {
              return {
                [dirname]: ls,
              };
            }

            return {
              [dirname]: merge(obj, ls),
            };
          }, null);

        self.setState({
          explorer,
        });
      }
    });
  }

  componentWillUnmount() {}

  render() {
    const {files, explorer} = this.state;

    const filesToShow = files.filter(f => !f.hidden);

    return <FileManagerView files={filesToShow} explorer={explorer} />;
  }
}

const mapStateToProps = state => {
  return {
    path: state.session.pwd,
  };
};

const mapActionsToProps = {};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(FileManager);
