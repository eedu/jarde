/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import classNames from 'classnames';

import {
  Button,
  Icon,
  Navbar,
  Elevation,
  Alignment,
  ButtonGroup,
  Tabs,
  Tab,
  Tree,
} from '@blueprintjs/core';

const tabid_prefix = 'filemanager-tab-';

class FileManagerView extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      selectedTabId: 0,
    };

    this.handleTabChange = this.handleTabChange.bind(this);
  }

  componentDidMount() {}

  componentWillUnmount() {}

  handleTabChange(selectedTabId) {
    selectedTabId = selectedTabId.split(tabid_prefix)[1];
    this.setState({selectedTabId});
  }

  toTreeNode(root, id = 0) {
    const hiddenFiles = false;
    return Object.keys(root)
      .map(key =>
        key.startsWith('.') && !hiddenFiles
          ? false
          : {
              id: id++,
              hasCaret: false,
              icon: root[key] ? 'folder-open' : 'folder-close',
              isExpanded: Boolean(root[key]),
              label: key,
              childNodes: root[key] ? this.toTreeNode(root[key], id) : null,
            },
      )
      .filter(e => Boolean(e));
  }

  render() {
    const {files, explorer} = this.props;

    const items = files.map(file => {
      var icon;
      const type = file.type.toLowerCase();

      switch (type) {
        case 'ascii text':
          icon = 'document';
          break;
        case 'data':
          icon = 'box';
          break;
        case 'empty':
          icon = 'square';
          break;
        case 'directory':
          icon = 'folder-open';
          break;
        default:
          if (type.indexOf('link') !== -1) {
            icon = 'link';
          } else if (type.indexOf('mail') !== -1) {
            icon = 'envelope';
          } else if (type.indexOf('audio') !== -1) {
            icon = 'music';
          } else if (type.indexOf('image') !== -1) {
            icon = 'media';
          } else if (
            type.indexOf('zip') !== -1 ||
            type.indexOf('binary') !== -1
          ) {
            icon = 'compressed';
          } else if (type.indexOf('script') !== -1) {
            icon = 'code';
          }
          break;
      }

      return (
        <div className="file">
          <Icon className="file-icon" icon={icon} iconSize={32} />
          <span className="filename">{file.name}</span>
        </div>
      );
    });

    // This is to force the flex behavior
    for (var i = 0; i < 10; i++) {
      items.push(<div className="file hidden"></div>);
    }

    const explorer_tree = this.toTreeNode(explorer);

    /*
    const explorer_tree = [
      {
        id: 0,
        hasCaret: false,
        icon: 'folder-close',
        label: 'Folder 0',
      },
      {
        id: 1,
        icon: 'folder-open',
        isExpanded: true,
        hasCaret: false,
        label: 'Folder 1',
        childNodes: [
          {
            id: 2,
            icon: 'document',
            label: 'Item 0',
          },
          {
            id: 4,
            hasCaret: false,
            icon: 'folder-open',
            isExpanded: true,
            label: 'Folder 0',
            childNodes: [
              {id: 5, label: 'No-Icon Item'},
              {id: 6, icon: 'tag', label: 'Item 1'},
              {
                id: 7,
                hasCaret: false,
                icon: 'folder-close',
                label: 'Folder 3',
                childNodes: [
                  {id: 8, icon: 'document', label: 'Item 0'},
                  {id: 9, icon: 'tag', label: 'Item 1'},
                ],
              },
            ],
          },
        ],
      },
      {
        id: 2,
        hasCaret: false,
        icon: 'folder-close',
        label: 'Super secret files',
        disabled: true,
      },
    ];
    */

    //const tabs = [ 'home', 'files', 'builds'];
    const tabs = [];

    return (
      <div className="file-manager">
        <Navbar className="files-header" elevation={Elevation.FOUR}>
          <Navbar.Group className="files-options" align={Alignment.LEFT}>
            <Navbar.Heading>
              <ButtonGroup className="navbar-buttons">
                <Button disabled icon="grid-view"></Button>
                <Button icon="align-justify"></Button>
              </ButtonGroup>
            </Navbar.Heading>
          </Navbar.Group>
          <Navbar.Group className="files-tabs" align={Alignment.LEFT}>
            <Tabs
              animate={true}
              id="filemanager-tabs"
              large={true}
              onChange={this.handleTabChange}
              selectedTabId={tabid_prefix + this.state.selectedTabId}>
              {tabs.map((tab, index) => (
                <Tab id={tabid_prefix + index} title={tab} />
              ))}
            </Tabs>
          </Navbar.Group>
          <Navbar.Group className="files-tabs-buttons" align={Alignment.RIGHT}>
            <Navbar.Heading>
              <Button icon="add"></Button>
            </Navbar.Heading>
          </Navbar.Group>
        </Navbar>
        <div className="files-explorer">
          <div className="files-sidebar">
            <Tree
              contents={explorer_tree}
              onNodeClick={null && this.handleNodeClick}
              onNodeCollapse={null && this.handleNodeCollapse}
              onNodeExpand={null && this.handleNodeExpand}
            />
          </div>
          <div className="files-grid bp3-elevation-4">{items}</div>
        </div>
      </div>
    );
  }
}

export default FileManagerView;
