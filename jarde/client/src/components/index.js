/* @format */

import FileManager from './file_manager';
import Terminal from './terminal';

const COMPONENTS = {
  FILE_MANAGER: 'file_manager',
  PREFERENCES: 'preferences',
  INFO: 'info',
  TERMINAL: 'terminal',
  COMMAND: 'command',
};

export function getRenderer(component) {
  switch (component) {
    case COMPONENTS.FILE_MANAGER:
      return FileManager;
    case COMPONENTS.PREFERENCES:
      return null;
    case COMPONENTS.INFO:
      return null;
    case COMPONENTS.TERMINAL:
      return Terminal;
    case COMPONENTS.COMMAND:
      return null;
    default:
      return null;
  }
}

export default COMPONENTS;
