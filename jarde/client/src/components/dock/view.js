/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import classNames from 'classnames';

import {
  Icon,
  Card,
  Elevation,
  Popover,
  PopoverInteractionKind,
  PopoverPosition,
  PopperBoundary,
  PopperModifiers,
} from '@blueprintjs/core';

const APPSTATE_ICONS = {
  running: 'dot',
};

class DockView extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    const {apps, onAppClick} = this.props;

    const items = apps.map(app => {
      const {id, name, component, icon, state} = app;

      const appStateIcon = false && state && APPSTATE_ICONS[state];

      return (
        <Popover
          interactionKind={PopoverInteractionKind.HOVER}
          modifiers={{
            arrow: {enabled: true},
            flip: {enabled: true},
            keepTogether: {enabled: true},
            preventOverflow: {enabled: false},
          }}
          hoverOpenDelay={100}
          hoverCloseDelay={150}
          position={PopoverPosition.RIGHT}>
          <div
            className={classNames('app', state)}
            onClick={e => onAppClick(app)}>
            <Icon icon={icon} iconSize={40} />
            {appStateIcon && <Icon icon={appStateIcon} iconSize={20} />}
          </div>
          <div className="dock-appname">
        {name}
          </div>
        </Popover>
      );
    });

    return (
      <Card className="dock" interactive={false} elevation={Elevation.THREE}>
        {items}
      </Card>
    );
  }
}

export default DockView;
