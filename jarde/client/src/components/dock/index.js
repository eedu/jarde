/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';

import classNames from 'classnames';

import ApplicationsStore from 'common/store/applications';

import DockView from './view';

import COMPONENTS from 'components';

const COMPONENTS_ICONS = {
  [COMPONENTS.FILE_MANAGER]: 'projects',
  [COMPONENTS.TERMINAL]: 'console',
  [COMPONENTS.PREFERENCES]: 'cog',
  [COMPONENTS.COMMAND]: 'console',
  [COMPONENTS.INFO]: 'info-sign',
};

class Dock extends React.Component {
  constructor(props) {
    super(props);

    this.openApp = this.openApp.bind(this);
  }

  componentDidMount() {}

  componentWillUnmount() {}

  openApp(app) {
    const {start} = this.props;

    start(app, {
      activate: true,
    });
  }

  render() {
    const {runningApps} = this.props;

    const favoriteApps = [
      {
        name: 'File Manager',
        component: COMPONENTS.FILE_MANAGER,
      },
      {
        name: 'Terminal',
        component: COMPONENTS.TERMINAL,
      },
      {
        name: 'Preferences',
        component: COMPONENTS.PREFERENCES,
      },
      {
        name: 'About',
        component: COMPONENTS.INFO,
      },
    ];

    const currentComponents = runningApps.map(e => e.component);
    const runningAppsNotOnFavorite = runningApps.filter(
      e => runningApps.filter(f => e.component === f.component).length === 0,
    );

    const apps = favoriteApps
      .map(e => {
        const runningApp = runningApps.filter(f => f.component === e.component);
        return runningApp.length ? runningApp[0] : e;
      })
      .concat(runningAppsNotOnFavorite)
      .map(app => ({
        name: app.name,
        component: app.component,
        icon: COMPONENTS_ICONS[app.component],
        state: app.state,
      }));

    return <DockView apps={apps} onAppClick={this.openApp} />;
  }
}

const mapStateToProps = state => {
  return {
    runningApps: Object.values(state.applications.running),
  };
};

const mapActionsToProps = {
  start: ApplicationsStore.actions.start,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Dock);
