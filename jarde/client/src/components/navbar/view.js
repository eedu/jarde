/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import classNames from 'classnames';

import {
  Boundary,
  Breadcrumbs,
  Card,
  Elevation,
  IBreadcrumbProps,
  Button,
  ButtonGroup,
  Navbar,
  Alignment,
  Icon,
} from '@blueprintjs/core';

class NavbarView extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    const {folders} = this.props;

    const items = folders.map((folder, i) => {
      switch (folder) {
        case 0:
          return <Button className="bp3-minimal" icon="globe" />;
        case 1:
          return <Button className="bp3-minimal" icon="home" />;
        default:
          if (i === folders.length - 1) {
            return (
              <Button text={folder} className="current-folder">
                <Icon icon="caret-down" />
              </Button>
            );
          }
          return <Button className="bp3-minimal" text={folder} />;
      }
    });

    return (
      <Navbar className="bp3-card navbar" elevation={Elevation.THREE}>
        <Navbar.Group align={Alignment.LEFT}>
          <Navbar.Heading>
            <ButtonGroup className="navbar-buttons">
              <Button icon="chevron-left"></Button>
              <Button icon="chevron-right"></Button>
            </ButtonGroup>
          </Navbar.Heading>
          {items}
        </Navbar.Group>
        <Navbar.Group align={Alignment.RIGHT}>
          <Button icon="bookmark"></Button>
        </Navbar.Group>
      </Navbar>
    );
  }
}

export default NavbarView;
