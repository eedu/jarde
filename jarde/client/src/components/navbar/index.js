/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';

import classNames from 'classnames';

import NavbarView from './view';

export const ENUM_PATH_SYMBOLS = {
  ROOT: 0,
  HOME: 1,
};

class Navbar extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    const {path, home} = this.props;

    var folders = [];

    path.split('/').forEach((folder, i) => {
      var f = '';
      if (i === 0) {
        f = ENUM_PATH_SYMBOLS.ROOT;
      } else if (path.split('/', i + 1).join('/') === home) {
        f = ENUM_PATH_SYMBOLS.HOME;
      } else {
        f = folder;
      }

      folders.push(f);
    });

    return <NavbarView folders={folders} />;
  }
}

const mapStateToProps = state => {
  return {
    path: state.session.pwd,
    home: state.session.home,
  };
};

const mapActionsToProps = {};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Navbar);
