/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import classNames from 'classnames';

import {
  Button,
  Icon,
  Navbar,
  Elevation,
  Alignment,
  ButtonGroup,
  Tabs,
  Tab,
  Tree,
} from '@blueprintjs/core';

class TerminalView extends React.Component {
  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    const {handleRef} = this.props;

    return <div className="terminal" ref={handleRef}></div>;
  }
}

export default TerminalView;
