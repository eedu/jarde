/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';

import {API_STATUS} from 'common/utils';
import execApi from 'common/api/exec';

import classNames from 'classnames';
import {merge, MergeParam, MERGE_ACTIONS} from 'common/functions/merge';

import {Terminal as XTerm, IEvent, IDisposable} from 'xterm';
import {FitAddon} from 'xterm-addon-fit';
import {AttachAddon} from 'xterm-addon-attach';

import ApplicationsStore from 'common/store/applications';

//import Term from 'react-xterm';
import TerminalView from './view';

class Terminal extends React.Component {
  constructor(props) {
    super(props);

    this.startTerm = this.startTerm.bind(this);
    this.startConnection = this.startConnection.bind(this);
  }

  componentDidMount() {
    const {appId, appData, create} = this.props;
    if (!appData.socket_path) {
      create(appId);
    }
  }

  componentWillUnmount() {}

  startTerm(div) {
    const fitAddon = new FitAddon();

    var term = new XTerm({
      cursorBlink: true,
      convertEol: true,
      fontFamily: `'Fira Mono', monospace`,
      fontSize: 15,
      fontWeight: 900,
    });

    this.term = term;

    term.setOption('theme', {
      background: 'black',
      foreground: 'white',
    });

    term.loadAddon(fitAddon);
    term.loadAddon(fitAddon);

    term.open(div);

    fitAddon.fit();

    //term.write('Hello from \x1B[1;3;31mxterm.js\x1B[0m $ ');

    /*
    term.onKey(key => {
      const char = key.domEvent.key;
      if (char === 'Enter') {
        this.prompt();
      } else if (char === 'Backspace') {
        term.write('\b \b');
      } else {
        term.write(char);
      }
    });
    */

    //this.prompt();
    this.startConnection();
    this.term.focus();
  }

  startConnection() {
    const {appData} = this.props;
    const {socket_path} = appData;

    const protocol = window.location.protocol === 'https:' ? 'wss://' : 'ws://';
    const port = window.location.port || '';
    const wsAddress = `${protocol}${window.location.hostname}:${port}/terminal/${socket_path}`;
    console.log(wsAddress);
    const connection = new WebSocket(wsAddress);
    this.connection = connection;
    const self = this;

    connection.onopen = ev => {
      const attachAddon = new AttachAddon(connection);
      self.term.loadAddon(attachAddon);
      connection.send('\n');
    };
  }

  prompt() {
    var shellprompt = '$ ';
    this.term.write('\r\n' + shellprompt);
  }

  render() {
    const {appId, appData} = this.props;

    if (!appData.socket_path) {
      return 'loading';
    }

    return <TerminalView handleRef={this.startTerm} />;
  }
}

const mapStateToProps = state => {
  return {
    path: state.session.pwd,
  };
};

const mapActionsToProps = {
  create: ApplicationsStore.actions.createTerminal,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Terminal);
