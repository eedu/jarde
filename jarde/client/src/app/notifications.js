/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';
import {merge, MergeParam, MERGE_ACTIONS} from 'common/functions/merge';

import {Toaster, Toast, Position, Intent} from '@blueprintjs/core';

import NotificationStore from 'common/store/notification';

export const Type = {
  ERROR: Intent.DANGER,
  SUCCESS: Intent.SUCCESS,
  INFO: Intent.PRIMARY,
  WARNING: Intent.WARNING,
};

class Notifications extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    const {setToaster} = this.props;
  }

  render() {
    const {removeNotification, notifications} = this.props;

    const timeout = 3000;

    return (
      <Toaster
        autoFocus={false}
        canEscapeKeyClear={true}
        position={Position.TOP}>
        {notifications.map(not => (
          <Toast
            message={not.message}
            intent={not.type}
            timeout={timeout}
            onDismiss={() => removeNotification(not.key)}
          />
        ))}
      </Toaster>
    );
  }
}

const mapStateToProps = state => {
  return {
    notifications: Object.keys(state.notification.notifications).map(key =>
      merge({key}, state.notification.notifications[key]),
    ),
  };
};

const mapActionsToProps = {
  removeNotification: NotificationStore.actions.remove,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Notifications);
