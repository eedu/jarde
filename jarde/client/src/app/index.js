/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';

import {Switch, Route, Redirect} from 'react-router-dom';

import RestrictedRoute from 'common/router/restricted';

import Router from 'common/router';

import Notifications from './notifications';
import AppView from './view';

import SessionStore from 'common/store/session';
import SystemStore from 'common/store/system';
import LocationStore from 'common/store/location';

class App extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    var {fetchSession, fetchSystem} = this.props;
    fetchSession();
    fetchSystem();
  }

  render() {
    const {session, preferences} = this.props;

    return (
      <AppView preferences={preferences}>
        <Notifications />
        <Router session={session} />
      </AppView>
    );
  }
}

const mapStateToProps = state => {
  return {
    session: state.session,
    preferences: state.preferences,
  };
};

const mapActionsToProps = {
  fetchSession: SessionStore.actions.fetchData,
  fetchSystem: SystemStore.actions.fetchData,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(App);
