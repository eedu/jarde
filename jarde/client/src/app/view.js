/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import classNames from 'classnames';

class AppView extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {preferences, children} = this.props;
    const {background, theme} = preferences;

    return (
      <div id="app" className={"theme-"+theme}>
        {children}
      </div>
    );
  }
}

export default AppView;
