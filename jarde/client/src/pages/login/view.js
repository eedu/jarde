/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import classNames from 'classnames';

import {
  Button,
  Classes,
  Code,
  H3,
  H5,
  Intent,
  Switch,
  MenuItem,
  InputGroup,
  Spinner,
} from '@blueprintjs/core';
import {Select} from '@blueprintjs/select';

import Modal from 'pages/modal';

class LoginView extends React.Component {
  constructor(props) {
    super(props);

    this.passwordRef = null;
  }

  componentDidMount() {}

  componentWillUnmount() {}

  itemRenderer(item, {handleClick, modifiers}) {
    return (
      <MenuItem
        active={modifiers.active}
        key={item}
        label={item}
        onClick={handleClick}
        text={item}
      />
    );
  }

  render() {
    const {
      ready,
      userlist,
      username,
      setUsername,
      password,
      setPassword,
      onLoginClick,
      isOpen,
      onClose,
    } = this.props;

    if (this.passwordRef) {
      this.passwordRef.focus();
    }

    /*
    <Select
      items={userlist}
      onItemSelect={e => setUsername(e)}
      itemRenderer={this.itemRenderer}
    />
    */

    return (
      <Modal
        onClose={onClose}
        isOpen={isOpen}
        loading={!ready}
        body={
          ready ? (
            <>
              <select
                name="username"
                value={username}
                onChange={e => setUsername(e.target.value)}>
                <option key="" value="" />
                {userlist.map(user => (
                  <option key={user} value={user}>
                    {user}
                  </option>
                ))}
              </select>
              <InputGroup
                value={password}
                placeholder="password"
                type="password"
                onChange={e => setPassword(e.target.value)}
                inputRef={e => (this.passwordRef = e)}
              />
            </>
          ) : (
            <Spinner />
          )
        }
        actions={
          ready ? (
            <Button
              icon="log-in"
              intent={Intent.PRIMARY}
              onClick={onLoginClick}>
              Login
            </Button>
          ) : null
        }
      />
    );
  }
}

export default LoginView;
