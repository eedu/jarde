/* @format */
import React, {useState} from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';
import {API_STATUS} from 'common/utils';

import LoginView from './view';

import {Type} from 'app/notifications';

import authApi from 'common/api/auth';
import SessionStore from 'common/store/session';
import LocationStore from 'common/store/location';
import NotificationStore from 'common/store/notification';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      ready: false,
    };

    this.login = this.login.bind(this);
    this.setUsername = this.setUsername.bind(this);
    this.setPassword = this.setPassword.bind(this);
    this.setReady = this.setReady.bind(this);
  }

  setUsername(username) {
    this.setState({username});
  }

  setPassword(password) {
    this.setState({password});
  }

  setReady(ready) {
    this.setState({ready});
  }

  login() {
    const {username, password} = this.state;
    const {attemptLogin, startTransition, update, notify} = this.props;
    //this.props.attemptLogin(username, password, promise => {
    this.setReady(false);
    const self = this;

    const promise = authApi.tryLogin(username, password);
    promise
      .then(data => {
        if (data.status === API_STATUS.SUCCESS) {
          startTransition();
          update(data.session);
        } else {
          self.setReady(true);
          notify({
            type: Type.ERROR,
            message: 'Wrong password',
          });
        }
      })
      .catch(err => {
        self.setReady(true);

        notify({
          type: Type.ERROR,
          message: 'Error trying to reach the server',
        });
      });
    //});
  }

  render() {
    const {isLeaving, leave, userlist} = this.props;
    const {ready} = this.state;

    if (userlist.length > 0 && this.state.username.length === 0) {
      this.setUsername(userlist[0]);
      this.setReady(true);
    }

    return (
      <LoginView
        ready={ready}
        onLoad={this.onLoad}
        userlist={userlist}
        username={this.state.username}
        setUsername={this.setUsername}
        password={this.state.password}
        setPassword={this.setPassword}
        onLoginClick={this.login}
        isOpen={!isLeaving}
        onClose={leave}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    loggedIn: state.session.loggedIn,
    systemLoaded: state.system.fetched,
    userlist: state.system.users,
  };
};

const mapActionsToProps = {
  attemptLogin: SessionStore.actions.attemptLogin,
  update: SessionStore.actions.update,
  startTransition: LocationStore.actions.startTransition,
  notify: NotificationStore.actions.create,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Login);
