/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';

import Navbar from 'components/navbar';
import FileManager from 'components/file_manager';
import Dock from 'components/dock';
import DesktopView from './view';
import JardeLayout from 'layouts/jarde';

import {getRenderer} from 'components';

class Desktop extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const {loaded, application, app_id} = this.props;
    const Layout = JardeLayout;

    if (!loaded) {
      return null;
    }

    const ApplicationComponent =
      application && getRenderer(application.component);

    return (
      <DesktopView
        isOpen={true}
        layout={Layout}
        navbar={<Navbar />}
        application={
          ApplicationComponent && (
            <ApplicationComponent appId={app_id} appData={application.data} />
          )
        }
        appState={application && application.state}
        dock={<Dock />}
      />
    );
  }
}

const mapStateToProps = state => {
  return {
    loaded: state.session.fetched,
    application: state.applications.running[state.applications.active],
    app_id: state.applications.active,
  };
};

const mapActionsToProps = {};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Desktop);
