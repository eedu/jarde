/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import classNames from 'classnames';

import {Button, Card, Elevation} from '@blueprintjs/core';

class DesktopView extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  componentWillUnmount() {}

  render() {
    var {layout, navbar, dock, application, isOpen, appState} = this.props;
    var Layout = layout;

    return (
      <Layout
        page="desktop"
        navbar={navbar}
        dock={dock}
        className={classNames({hasActiveApp: Boolean(application)})}
        appState={appState}
        application={
          application && (
            <Card
              className="container"
              interactive={false}
              elevation={Elevation.THREE}>
              {application}
            </Card>
          )
        }
      />
    );
  }
}

export default DesktopView;
