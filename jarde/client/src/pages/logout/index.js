/* @format */
import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';

class Logout extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return <div>Logout</div>;
  }
}

const mapStateToProps = state => {
  return { data: state };
};

const mapActionsToProps = {};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(Logout);
