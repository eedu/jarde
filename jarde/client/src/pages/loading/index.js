/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';

import {
  Button,
  Classes,
  Code,
  H3,
  H5,
  Intent,
  Dialog,
  Switch,
  Spinner,
} from '@blueprintjs/core';

class Loading extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const isOpen = true;

    return <Spinner />;
  }
}

const mapStateToProps = state => {};

const mapActionsToProps = {};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Loading);
