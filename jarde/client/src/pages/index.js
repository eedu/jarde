/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';

import LocationStore from 'common/store/location';

class Page extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    const {component, isTransitioning, finishTransition} = this.props;
    const Component = component;

    return (
      <div className="page overlay">
        <Component isLeaving={isTransitioning} leave={finishTransition} />
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    isTransitioning: state.location.isTransitioning,
  };
};

const mapActionsToProps = {
  finishTransition: LocationStore.actions.endTransition,
};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Page);
