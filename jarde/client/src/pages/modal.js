/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';

import classNames from 'classnames';

import {Classes, Dialog} from '@blueprintjs/core';

class Modal extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    const {preferences, body, actions, onClose, isOpen, loading} = this.props;
    const {theme} = preferences;

    return (
      <Dialog
        className={classNames('dialog modal theme-' + theme, {loading})}
        canOutsideClickClose={false}
        canEscapeKeyClose={false}
        isCloseButtonShown={false}
        onClosed={onClose}
        isOpen={isOpen}>
        <div className={Classes.DIALOG_BODY}>{body}</div>
        <div
          className={classNames(
            Classes.DIALOG_FOOTER,
            Classes.DIALOG_FOOTER_ACTIONS,
          )}>
          {actions}
        </div>
      </Dialog>
    );
  }
}

const mapStateToProps = state => {
  return {
    preferences: state.preferences,
  };
};

const mapActionsToProps = {};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Modal);
