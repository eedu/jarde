/* @format */

const LocalStorage = {
  save(storage_key, state) {
    var p1 = new Promise((resolve, reject) => {
      try {
        const serializedState = JSON.stringify(state);
        localStorage.setItem(storage_key, serializedState);
        resolve();
      } catch (e) {
        console.log(e);
        reject();
      }
    });

    return p1;
  },

  load(storage_key) {
    var p1 = new Promise((resolve, reject) => {
      try {
        const serializedState = localStorage.getItem('state');
        if (serializedState === null) {
          resolve(undefined);
        }

        resolve(JSON.parse(serializedState));
      } catch (e) {
        console.log(e);
        reject();
      }
    });

    return p1;
  },
};

export default LocalStorage;
