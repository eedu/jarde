/* @format */
import { merge, MergeParam, MERGE_ACTIONS } from 'common/functions/merge';

import { SET_AGENTS, SET_QUEUES, SET_AUDIO_FILES, SET_IVRS } from './actions';

function setAgents(store, payload) {
  const { list } = payload;

  return merge(store, {
    agents: new MergeParam(MERGE_ACTIONS.SET, list),
  });
}

function setQueues(store, payload) {
  const { list } = payload;

  return merge(store, {
    queues: new MergeParam(MERGE_ACTIONS.SET, list),
  });
}

function setAudios(store, payload) {
  const { list } = payload;

  return merge(store, {
    audios: new MergeParam(MERGE_ACTIONS.SET, list),
  });
}

function setIVRs(store, payload) {
  const { list } = payload;

  return merge(store, {
    ivr: {
      list: new MergeParam(MERGE_ACTIONS.SET, list),
    },
  });
}

function clean() {
  return {
    agents: {},
    queues: {},
    audios: {},
    ivr: {
      list: [],
    },
  };
}

function init() {
  return clean();
}

function externalReducer(store = null, { type, payload }) {
  if (!store) {
    return init();
  }

  switch (type) {
    case SET_AGENTS:
      return setAgents(store, payload);
    case SET_QUEUES:
      return setQueues(store, payload);
    case SET_AUDIO_FILES:
      return setAudios(store, payload);
    case SET_IVRS:
      return setIVRs(store, payload);
    default:
      return store;
  }
}

export default externalReducer;
