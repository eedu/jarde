/* @format */
import { merge, MergeParam, MERGE_ACTIONS } from 'common/functions/merge';

import sessionApi from '../session';

export function actionFetchSessionData() {
  return (dispatch, getState) => {
    return sessionApi.read().then(data => {
      console.log(data);
    });
  };
}
