/* @format */
import axios from 'axios';
import config from './config';

import LocalStorage from 'common/localStorage';

export default {
  info() {
    return axios
      .get('/systeminfo', config.defaultRequestOptions)
      .then(({data}) => data);
  },
};
