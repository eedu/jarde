/* @format */
import axios from 'axios';
import config from './config';

export default {
  listDirectories(input) {
    return axios
      .post('/exec/list-directories', {input}, config.defaultRequestOptions)
      .then(({data}) => data);
  },
  ls(input) {
    return axios
      .post('/exec/ls', {input}, config.defaultRequestOptions)
      .then(({data}) => data);
  },
};
