/* @format */
import axios from 'axios';
import config from './config';

export default {
  tryLogin(username, password) {
    return axios
      .post('/login', {username, password}, config.defaultRequestOptions)
      .then(({data}) => data);
  },
};
