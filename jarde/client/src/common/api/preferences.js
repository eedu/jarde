/* @format */
import axios from 'axios';
import config from './config';

import LocalStorage from 'common/localStorage';

export default {
  read() {
    return LocalStorage.read('preferences');
  },

  write(state) {
    return LocalStorage.save('preferences', state);
  },
};
