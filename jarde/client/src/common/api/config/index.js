/* @format */

export default {
  defaultRequestOptions: {
    headers: {
      'Content-Type': 'application/json',
    },
  },
};
