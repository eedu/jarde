/* @format */
import axios from 'axios';
import config from './config';

export default {
  getBackgrounds() {
    return axios
      .post('/misc/backgrounds', config.defaultRequestOptions)
      .then(({data}) => data);
  },
};
