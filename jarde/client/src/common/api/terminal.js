/* @format */
import axios from 'axios';
import config from './config';

export default {
  create() {
    return axios
      .post('/terminal/create', config.defaultRequestOptions)
      .then(({data}) => data);
  },
};
