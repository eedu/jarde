/* @format */
import axios from 'axios';
import config from './config';

export default {
  read() {
    return axios
      .get('/session', config.defaultRequestOptions)
      .then(({ data }) => data);
  },
};
