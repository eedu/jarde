const defaultOptions = {
  delete_undefined: false,
};

export function merge(el1, el2, options = defaultOptions) {
  const obj = mergeRecursive(el1, el2, options);
  return obj;
}

export const MERGE_ACTIONS = {
  SET: 'mergeParam:set',
  INSERT: 'mergeParam:insert',
  MOVE: 'mergeParam:move',
  INCREMENT: 'mergeParam:increment',
  PUSH: 'mergeParam:push',
  DELETE: 'mergeParam:delete',
};

const interactableConstructors = [Array, Object];

export class MergeParam {
  constructor(operation, object) {
    this.operation = typeof operation === 'object' ? operation[0] : operation;
    this.params =
      typeof operation === 'object'
        ? {
            pos: operation[1],
          }
        : null;
    this.object = object;
    this.isMergeParam = true;
  }

  doOperation(el1, el2) {
    const { object } = el2;
    const pos = this.params && this.params.pos;

    switch (this.operation) {
      case MERGE_ACTIONS.SET:
        return object;

      case MERGE_ACTIONS.PUSH:
        if (!el1 || !el1.length) {
          return [object];
        }

        return el1.concat(object);

      case MERGE_ACTIONS.INSERT:
        if (!el1 || !el1.length) {
          return [object];
        }
        return pos === -1
          ? el1.concat(object)
          : el1
              .slice(0, pos)
              .concat(object)
              .concat(el1.slice(pos));

      case MERGE_ACTIONS.MOVE:
        if (!el1 || !el1.length) {
          return [object];
        }

        const [oldIndex, newIndex] = pos;

        const afterDelete = el1
          .slice(0, oldIndex)
          .concat(el1.slice(oldIndex + 1));
        const afterInsert =
          newIndex === -1
            ? el1.concat(object)
            : el1
                .slice(0, newIndex)
                .concat(object)
                .concat(el1.slice(newIndex));

        return afterInsert;

      case MERGE_ACTIONS.DELETE:
        if (
          typeof object === 'number' &&
          typeof el1 === 'object' &&
          el1.constructor === Array
        ) {
          const index = object;

          return el1.slice(0, index).concat(el1.slice(index + 1));
        }

        return undefined;

      case MERGE_ACTIONS.INCREMENT:
        return typeof object === 'number' ? object + 1 : el1 + 1;
      default:
        console.log('ERROR');
        debugger;
    }
  }
}

function isMergeParam(el) {
  return (
    typeof el === 'object' && (el.constructor === MergeParam || el.isMergeParam)
  );
}

function mergeRecursive(el1, el2, options) {
  if (el2 && typeof el2 === 'object' && isMergeParam(el2)) {
    return el2.doOperation(el1, el2);
  }

  if (el2 === null || el2 === undefined) {
    return el1;
  }

  if (
    typeof el2 === 'object' &&
    interactableConstructors.indexOf(el2.constructor) === -1
  ) {
    return el2;
  }

  if (el1 === null || el1 === undefined || typeof el1 !== 'object') {
    el1 = {};
  }

  var obj = Object.assign({}, el1);

  if (typeof el2 === 'object' && el2.constructor !== Array) {
    Object.keys(el2).forEach(key => {
      obj[key] = mergeRecursive(el1[key], el2[key], options);
      if (obj[key] === undefined && options.delete_undefined) {
        delete obj[key];
      }
    });
  } else {
    obj = el2;
  }

  return obj;
}
