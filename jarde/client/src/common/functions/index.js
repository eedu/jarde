export function setPath(obj, [key, ...next], value) {
  if (next.length === 0) {
    return { ...obj, [key]: value };
  }
  return { ...obj, [key]: setPath(obj[key], next, value) };
}

export function getPath(obj, [key, ...next]) {
  if (next.length === 0) {
    return obj[key];
  }
  return getPath(obj[key], next);
}

export function simpleDiff(obj1, obj2) {
  return Object.keys(obj1).filter(e => {
    return obj2[e] === undefined;
  });
}
