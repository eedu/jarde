export default function uniqueKey(keysUsed) {
  var key = null;
  while (!key || keysUsed.indexOf(key) !== -1) {
    key = Math.floor(Math.random() * Number.MAX_SAFE_INTEGER);
  }

  return key;
}
