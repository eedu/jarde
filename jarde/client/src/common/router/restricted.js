/* @format */
import React from 'react';
import { Route, Redirect } from 'react-router-dom';

class RestrictedRoute extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { children, butIf, redirectTo, ...rest } = this.props;
    //const Component = component;
    return (
      <Route {...rest}>{butIf ? <Redirect to={redirectTo} /> : children}</Route>
    );
  }
}

export default RestrictedRoute;
