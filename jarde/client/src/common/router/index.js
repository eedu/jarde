/* @format */
import React from 'react';
import {PropTypes} from 'prop-types';
import {connect} from 'react-redux';

import {Switch, Route, Redirect} from 'react-router-dom';

import RestrictedRoute from 'common/router/restricted';

import Page from 'pages';
import Login from 'pages/login';
import Desktop from 'pages/desktop';
import Logout from 'pages/logout';
import Loading from 'pages/loading';

const routes = [
  {
    path: '/',
    exact: true,
    redirect: () => '/login',
    component: null,
  },
  {
    path: '/desktop',
    exact: true,
    redirect: props => {
      return !props.loggedIn ? '/login' : false;
    },
    component: Desktop,
  },
  {
    path: '/login',
    exact: true,
    redirect: props => {
      return props.loggedIn ? '/desktop' : false;
    },
    component: Login,
  },
  {
    path: '/logout',
    exact: true,
    redirect: props => false,
    component: Logout,
  },
];

class Router extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {}

  render() {
    const {isTransitioning, session} = this.props;

    return (
      <Switch>
        {routes.map(route => {
          var redirect = route.redirect(session);
          return session.fetched ? (
            <RestrictedRoute
              exact={route.exact}
              path={route.path}
              butIf={!isTransitioning && redirect}
              redirectTo={redirect}>
              <Page component={route.component} />
            </RestrictedRoute>
          ) : (
            <Route exact={route.exact} path={route.path}>
              <Page component={Loading} />
            </Route>
          );
        })}
      </Switch>
    );
  }
}

const mapStateToProps = state => {
  return {
    isTransitioning: state.location.isTransitioning,
  };
};

const mapActionsToProps = {};

export default connect(
  mapStateToProps,
  mapActionsToProps,
)(Router);
