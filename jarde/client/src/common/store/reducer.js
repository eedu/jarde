/* @format */
import { combineReducers } from 'redux';
import { merge } from 'common/functions/merge';

import config from './config';

console.log(config);

var reducers = config.stores.reduce((obj, item) => {
  return merge(obj, {
    [item.name]: item.reducer,
  });
}, {});

const subReducers = combineReducers(reducers);

function appReducer(store = null, { type, payload }) {
  switch (type) {
    default:
      return store;
  }
}

const allReducers = (state, action) => {
  return subReducers(appReducer(state, action), action);
};

export default allReducers;
