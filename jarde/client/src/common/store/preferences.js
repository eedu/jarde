/* @format */
import {merge, MergeParam, MERGE_ACTIONS} from 'common/functions/merge';
import preferencesApi from 'common/api/preferences';
import {API_STATUS} from 'common/utils';

const ACTION_REFS = {
  UPDATE: 'preferences:update',
  CLEAR: 'preferences:clear',
};

const INITIAL_STATE = {
  background: 'default',
  locale: 'en-US',
  theme: 'light',
  layout: 'jarde',
};

const ACTIONS = {
  update(pref) {
    return {
      type: ACTION_REFS.UPDATE,
      payload: pref,
    };
  },

  load() {
    return dispatch => {
      return preferencesApi.read().then(data => {
        dispatch(ACTIONS.update(data));
      });
    };
  },
};

const PreferencesStore = {
  actionRefs: ACTION_REFS,
  actions: ACTIONS,
  initialState: INITIAL_STATE,

  update(store, payload) {
    const {data} = store;

    const newState = merge(store, payload, {delete_undefined: true});

    preferencesApi.write(newState);

    return newState;
  },

  clear(store, payload) {
    return PreferencesStore.initialState;
  },

  init() {
    return PreferencesStore.clear();
  },

  reducer(store = null, {type, payload}) {
    if (!store) {
      return PreferencesStore.init();
    }

    switch (type) {
      case ACTION_REFS.CLEAR:
        return PreferencesStore.clear(store, payload);
      case ACTION_REFS.UPDATE:
        return PreferencesStore.update(store, payload);
      default:
        return store;
    }
  },
};
export default PreferencesStore;
