/* @format */
import {merge, MergeParam, MERGE_ACTIONS} from 'common/functions/merge';
import sessionApi from 'common/api/session';
import authApi from 'common/api/auth';
import {API_STATUS} from 'common/utils';

import LocationStore from 'common/store/location';

const ACTION_REFS = {
  UPDATE: 'session:update',
  CLEAR: 'session:clear',
  LOGIN: 'session:login',
  LOGOUT: 'session:login',
};

const INITIAL_STATE = {
  fetched: false,
  username: null,
  date: null,
  loggedIn: false,
};

const ACTIONS = {
  update(session) {
    return {
      type: ACTION_REFS.UPDATE,
      payload: {
        session,
      },
    };
  },

  fetchData() {
    return dispatch => {
      return sessionApi.read().then(data => {
        if (data.status === API_STATUS.SUCCESS) {
          dispatch(ACTIONS.update(data.session));
        }
      });
    };
  },

  setLoggedIn() {
    return {
      type: ACTION_REFS.LOGIN,
      payload: null,
    };
  },

  setLoggedOut() {
    return {
      type: ACTION_REFS.LOGOUT,
      payload: null,
    };
  },

  attemptLogin(username, password, callback) {
    const promise = authApi.tryLogin(username, password);
    callback(promise);
  },
};

const SessionStore = {
  actionRefs: ACTION_REFS,
  actions: ACTIONS,
  initialState: INITIAL_STATE,

  update(store, payload) {
    const {data} = store;
    const {session} = payload;

    var newState = merge(store, session, {delete_undefined: true});
    newState = merge(newState, {fetched: true}, {delete_undefined: true});

    return newState;
  },

  login(store, payload) {
    return merge(
      store,
      {
        loggedIn: true,
      },
      {delete_undefined: true},
    );
  },

  logout(store, payload) {
    return merge(
      store,
      {
        loggedIn: false,
      },
      {delete_undefined: true},
    );
  },

  clear(store, payload) {
    return SessionStore.initialState;
  },

  init() {
    return SessionStore.clear();
  },

  reducer(store = null, {type, payload}) {
    if (!store) {
      return SessionStore.init();
    }

    switch (type) {
      case ACTION_REFS.CLEAR:
        return SessionStore.clear(store, payload);
      case ACTION_REFS.UPDATE:
        return SessionStore.update(store, payload);
      case ACTION_REFS.LOGIN:
        return SessionStore.login(store, payload);
      case ACTION_REFS.LOGOUT:
        return SessionStore.logout(store, payload);
      default:
        return store;
    }
  },
};
export default SessionStore;
