/* @format */
import {merge, MergeParam, MERGE_ACTIONS} from 'common/functions/merge';
import {API_STATUS} from 'common/utils';
import terminalApi from 'common/api/terminal';

const ACTION_REFS = {
  START: 'applications:start',
  UPDATE: 'applications:update',
  CLEAR: 'applications:clear',
};

const INITIAL_STATE = {
  running: {},
  active: null,
};

const ACTIONS = {
  update(pid, data) {
    return {
      type: ACTION_REFS.UPDATE,
      payload: {pid, data},
    };
  },

  start(app, options) {
    return {
      type: ACTION_REFS.START,
      payload: {app, options},
    };
  },

  createTerminal(appId) {
    return dispatch => {
      return terminalApi.create().then(data => {
        if (data.status === API_STATUS.SUCCESS) {
          dispatch(ACTIONS.update(appId, data));
        }
      });
    };
  },
};

const ApplicationsStore = {
  actionRefs: ACTION_REFS,
  actions: ACTIONS,
  initialState: INITIAL_STATE,

  start(store, payload) {
    const {app, options} = payload;
    const {name, component, state, data} = app;
    const {activate} = options;
    const currentKeys = Object.keys(store.running);
    const pid = currentKeys.length === 0 ? 1 : Math.max(currentKeys) + 1;

    const newState = merge(
      store,
      {
        running: {
          [pid]: new MergeParam(MERGE_ACTIONS.SET, {
            name,
            component,
            state: state || 'opening',
            data: data || {},
          }),
        },
        active: activate ? pid : store.active,
      },
      {delete_undefined: true},
    );

    return newState;
  },

  update(store, payload) {
    const {pid, data} = payload;
    if (data.pid) {
      delete data.pid;
    }

    const newState = merge(
      store,
      {
        running: {
          [pid]: {
            data,
          },
        },
      },
      {delete_undefined: true},
    );

    return newState;
  },

  clear(store, payload) {
    return ApplicationsStore.initialState;
  },

  init() {
    return ApplicationsStore.clear();
  },

  reducer(store = null, {type, payload}) {
    if (!store) {
      return ApplicationsStore.init();
    }

    switch (type) {
      case ACTION_REFS.CLEAR:
        return ApplicationsStore.clear(store, payload);
      case ACTION_REFS.START:
        return ApplicationsStore.start(store, payload);
      case ACTION_REFS.UPDATE:
        return ApplicationsStore.update(store, payload);
      default:
        return store;
    }
  },
};
export default ApplicationsStore;
