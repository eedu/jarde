import ApplicationsStore from './applications';
import NotificationStore from './notification';
import PreferencesStore from './preferences';
import SessionStore from './session';
import LocationStore from './location';
import SystemStore from './system';

const config = {
  stores: [
    {
      name: 'applications',
      reducer: ApplicationsStore.reducer,
    },
    {
      name: 'notification',
      reducer: NotificationStore.reducer,
    },
    {
      name: 'preferences',
      reducer: PreferencesStore.reducer,
    },
    {
      name: 'system',
      reducer: SystemStore.reducer,
    },
    {
      name: 'session',
      reducer: SessionStore.reducer,
    },
    {
      name: 'location',
      reducer: LocationStore.reducer,
    },
  ],
};

export default config;
