/* @format */
import { createStore, applyMiddleware, compose } from 'redux';
import asyncDispatch from './middlewares/asyncDispatch';
import thunk from 'redux-thunk';

import appReducer from './reducer';

const defaultInitialState = {};

function saveToLocalStorage(storage_key, state) {
  try {
    const serializedState = JSON.stringify(state);
    localStorage.setItem(storage_key, serializedState);
  } catch (e) {
    console.log(e);
  }
}

function loadFromLocalStorage(storage_key) {
  try {
    const serializedState = localStorage.getItem('state');
    if (serializedState === null) {
      return undefined;
    }

    return JSON.parse(serializedState);
  } catch (e) {
    console.log(e);
  }
}

const state = {};

const store = createStore(
  appReducer,
  state,
  compose(
    //applyMiddleware(asyncDispatch),
    applyMiddleware(thunk),
    window.__REDUX_DEVTOOLS_EXTENSION__
      ? window.__REDUX_DEVTOOLS_EXTENSION__()
      : f => f
  )
);

/*
if (SHOULD_USE_LOCAL_STORAGE) {
    store.subscribe(() => saveToLocalStorage(store.getState()));
}
*/

export default store;
