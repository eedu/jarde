/* @format */
import {merge, MergeParam, MERGE_ACTIONS} from 'common/functions/merge';
import {useHistory} from 'react-router-dom';

const LOCATIONS = {
  DESKTOP: '/desktop',
  LOGIN: '/login',
  LOGOUT: '/logout',
  ROOT: '/',
};

const ACTION_REFS = {
  START_TRANSITION: 'location:start-transition',
  END_TRANSITION: 'location:end-transition',
  GOTO: 'location:goto',
  SET: 'location:set',
  RESET: 'location:reset',
};

const INITIAL_STATE = {
  url: LOCATIONS.ROOT,
  isTransitioning: false,
};

const ACTIONS = {
  startTransition() {
    return {
      type: ACTION_REFS.START_TRANSITION,
      payload: {},
    };
  },

  endTransition() {
    return {
      type: ACTION_REFS.END_TRANSITION,
      payload: {},
    };
  },

  goTo(url) {
    return {
      type: ACTION_REFS.GOTO,
      payload: {url},
    };
  },

  set(url) {
    return {
      type: ACTION_REFS.SET,
      payload: {url},
    };
  },

  reset() {
    return {
      type: ACTION_REFS.RESET,
      payload: null,
    };
  },
};

const LocationStore = {
  actionRefs: ACTION_REFS,
  actions: ACTIONS,
  initialState: INITIAL_STATE,
  locations: LOCATIONS,

  startTransition(store, payload) {
    return merge(store, {
      isTransitioning: true,
    });
  },

  endTransition(store, payload) {
    return merge(store, {
      isTransitioning: false,
    });
  },

  goTo(store, payload) {
    const {url} = payload;

    return LocationStore.set(store, payload);
  },

  set(store, payload) {
    const {url} = payload;

    return merge(store, {
      url,
    });
  },

  reset(store, payload) {
    return LocationStore.initialState;
  },

  init() {
    return LocationStore.reset();
  },

  reducer(store = null, {type, payload}) {
    if (!store) {
      return LocationStore.init();
    }

    switch (type) {
      case ACTION_REFS.START_TRANSITION:
        return LocationStore.startTransition(store, payload);
      case ACTION_REFS.END_TRANSITION:
        return LocationStore.endTransition(store, payload);
      case ACTION_REFS.SET:
        return LocationStore.set(store, payload);
      case ACTION_REFS.RESET:
        return LocationStore.reset(store, payload);
      default:
        return store;
    }
  },
};
export default LocationStore;
