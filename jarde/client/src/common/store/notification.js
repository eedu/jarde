/* @format */
import {merge, MergeParam, MERGE_ACTIONS} from 'common/functions/merge';

const ACTION_REFS = {
  CREATE: 'notification:create',
  REMOVE: 'notification:remove',
};

const INITIAL_STATE = {
  notifications: {},
};

const ACTIONS = {
  create(notification) {
    return {
      type: ACTION_REFS.CREATE,
      payload: {notification},
    };
  },
  remove(key) {
    return {
      type: ACTION_REFS.REMOVE,
      payload: {key},
    };
  },
};

const NotificationStore = {
  actionRefs: ACTION_REFS,
  actions: ACTIONS,
  initialState: INITIAL_STATE,

  create(store, payload) {
    const {notifications} = store;
    const {notification} = payload;

    const currentKeys = Object.keys(notifications);
    const newKey = currentKeys.length ? Math.max(currentKeys) + 1 : 0;

    return merge(
      store,
      {
        notifications: {
          [newKey]: notification,
        },
      },
      {delete_undefined: true},
    );
  },

  remove(store, payload) {
    const {notifications} = store;
    const {key} = payload;

    return merge(
      store,
      {
        notifications: {
          [key]: new MergeParam(MERGE_ACTIONS.DELETE),
        },
      },
      {delete_undefined: true},
    );
  },

  clear(store, payload) {
    return NotificationStore.initialState;
  },

  init() {
    return NotificationStore.clear();
  },

  reducer(store = null, {type, payload}) {
    if (!store) {
      return NotificationStore.init();
    }

    switch (type) {
      case ACTION_REFS.CREATE:
        return NotificationStore.create(store, payload);
      case ACTION_REFS.REMOVE:
        return NotificationStore.remove(store, payload);
      default:
        return store;
    }
  },
};
export default NotificationStore;
