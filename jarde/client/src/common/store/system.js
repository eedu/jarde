/* @format */
import {merge, MergeParam, MERGE_ACTIONS} from 'common/functions/merge';
import systemApi from 'common/api/system';
import {API_STATUS} from 'common/utils';

const ACTION_REFS = {
  UPDATE: 'system:update',
  CLEAR: 'system:clear',
};

const INITIAL_STATE = {
  fetched: false,
  users: [],
};

const ACTIONS = {
  update(pref) {
    return {
      type: ACTION_REFS.UPDATE,
      payload: pref,
    };
  },

  fetchData() {
    return dispatch => {
      return systemApi.info().then(data => {
        if (data.status === API_STATUS.SUCCESS) {
          dispatch(ACTIONS.update(data.sysinfo));
        }
      });
    };
  },
};

const SystemStore = {
  actionRefs: ACTION_REFS,
  actions: ACTIONS,
  initialState: INITIAL_STATE,

  update(store, payload) {
    const {data} = store;

    var newState = merge(store, payload, {delete_undefined: true});
    newState = merge(newState, {fetched: true}, {delete_undefined: true});

    return newState;
  },

  clear(store, payload) {
    return SystemStore.initialState;
  },

  init() {
    return SystemStore.clear();
  },

  reducer(store = null, {type, payload}) {
    if (!store) {
      return SystemStore.init();
    }

    switch (type) {
      case ACTION_REFS.CLEAR:
        return SystemStore.clear(store, payload);
      case ACTION_REFS.UPDATE:
        return SystemStore.update(store, payload);
      default:
        return store;
    }
  },
};
export default SystemStore;
