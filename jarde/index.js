/* @format */

const WebSocket = require('ws');
const express = require('express');
const fs = require('fs');
const http = require('http');
const https = require('https');
//const privateKey = fs.readFileSync('./server/ca/server.key', 'utf8');
//const certificate = fs.readFileSync('./server/ca/server.crt', 'utf8');

const options = {
  //key: privateKey,
  //cert: certificate,
  //requestCert: false,
  //rejectUnauthorized: false,
};

const app = express();

var middlewares = require('./server/middlewares'); //importing route
const routes = require('./server/routes'); //importing route

//var httpServer = http.createServer(app);
var server = http.createServer(options, app);

middlewares(app, server);
routes(app, server);

const port = 8080;
server.listen(port);

//app.listen(port);

console.log('App is listening on port ' + port);
