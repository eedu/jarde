/* @format */
'use strict';

const system = require('../utils/system');
const pty = require('node-pty');
const WebSocket = require('ws');

const crypto = require('crypto');
const hash = crypto.createHash('sha256');

const handle_connection = (ws, req, shell) => {
  // For all shell data send it to the websocket
  shell.on('data', data => {
    ws.send(data);
  });
  // For all websocket data send it to the shell
  ws.on('message', msg => {
    shell.write(msg);
  });
};

exports.instantiate = (session, sessionID, server) => {
  return new Promise((resolve, reject) => {
    const shell = pty.spawn(process.env.SHELL, [], {
      name: 'xterm-color',
      cwd: process.env.PWD,
      env: process.env,
    });

    if (!session.terminals) {
      session.terminals = {};
    }

    hash.update(sessionID + Object.keys(session.terminals).join(','));
    const socket_path = hash.digest('hex');

    const terminalWss = new WebSocket.Server({
      server,
      path: `/terminal/${socket_path}`,
    });
    terminalWss.on('connection', (ws, req) =>
      handle_connection(ws, req, shell),
    );

    session.terminals[socket_path] = terminalWss;

    resolve(socket_path);
  });
};

exports.open = session => {
  return new Promise((resolve, reject) => {
    resolve();
  });
};
