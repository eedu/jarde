/* @format */
'use strict';

const proxy = require('../utils/proxy.js');
const bo = require('./bo');
const pty = require('node-pty');

exports.instantiate = (req, res) => {
  const {script, input} = req.body;

  var resp = {
    status: 500,
    socket_path: null,
  };

  bo.instantiate(req.session, req.sessionID, req.server_instance)
    .then(output => {
      resp.status = 200;
      resp.socket_path = output;
      res.json(resp);
    })
    .catch(err => {
      console.log(err);
      resp.status = 404;
      res.json(resp);
    });
};
