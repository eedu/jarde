/* @format */

'use strict';

const express = require('express');
const path = require('path');

module.exports = (app, expressWs) => {
  var terminal_controller = require('../terminal/');
  var session_controller = require('../session/');
  var process_controller = require('../process/');
  var system_controller = require('../system/');

  app.route('/login').post(session_controller.login);

  app.route('/logout').post(session_controller.logout);

  app.route('/session').get(session_controller.get_session);

  app.route('/systeminfo').get(system_controller.info);

  app.route('/logout').post(session_controller.logout);

  app.route('/terminal/create').post(terminal_controller.instantiate);

  app.route('/exec/:command').post(process_controller.exec);

  app.post('/exec/:command', process_controller.exec);

  app.route('/api/process/new/:command').get(process_controller.create);

  app.post('*', (req, res) => {
    console.log('problem with url: ' + req.url);
  });

  app.use(express.static(path.join(process.cwd(), 'jarde/client/build')));
  // Handles any requests that don't match the ones above
  app.get('/node_modules/*', (req, res) => {
    var url = req.url;
    res.sendFile(path.join(process.cwd() + '/jarde/client/' + url));
  });

  app.get('/modules/*', (req, res) => {
    var url = req.url
      .replace('/modules', 'node_modules')
      .replace('node_modules/node_modules', 'node_modules');
    res.sendFile(path.join(process.cwd() + '/jarde/client/' + url));
  });
  app.get('*', (req, res) => {
    console.log('looking for file', req.url);
    res.sendFile(path.join(process.cwd() + '/jarde/client/build/index.html')); //'+req.url));
  });
};
