/* @format */
'use strict';
const urls = [
  {
    url: '/',
    exact: true,
  },
  {
    url: '/systeminfo',
    exact: true,
  },
  {
    url: '/session/',
    exact: false,
  },
  {
    url: '/login/',
    exact: false,
  },
  {
    url: '/desktop/',
    exact: false,
  },
  {
    url: '/logout/',
    exact: false,
  },
  {
    url: '/public/',
    exact: false,
  },
  {
    url: '/vendor/',
    exact: false,
  },
  {
    url: '/node_modules/',
    exact: false,
  },
  {
    url: '/modules/',
    exact: false,
  },
  {
    url: '/theme/',
    exact: false,
  },
  {
    url: '/static/',
    exact: false,
  },
  {
    url: '/favicon.ico',
    exact: true,
  },
];

exports.checkUrl = url => {
  var res = false;
  urls.forEach(u => {
    if (u.exact) {
      if (u.url === url) {
        res = true;
      }
    } else {
      if ((url + '/').startsWith(u.url)) {
        res = true;
      }
    }
  });

  return res;
};
