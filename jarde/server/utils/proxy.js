/* @format */
const http = require('http');

exports.request = (
  client_req,
  client_res,
  hostname,
  port,
  path_replace = null,
) => {
  var path = '' + client_req.url;

  if (path_replace) {
    Object.keys(path_replace).forEach(key => {
      path = path.replace(key, path_replace[key]);
    });
  }

  console.log('redirecting', client_req.path, 'to ', path);

  var options = {
    hostname,
    port,
    path: path,
    method: client_req.method,
    headers: client_req.headers,
  };

  var proxy = http.request(options, function(res) {
    client_res.writeHead(res.statusCode, res.headers);
    res.pipe(
      client_res,
      {
        end: true,
      },
    );
  });

  client_req.pipe(
    proxy,
    {
      end: true,
    },
  );
};
