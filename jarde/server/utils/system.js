/* @format */

const util = require('util');
const exec = require('child_process').exec;
const promisseExec = util.promisify(exec);

exports.exec = cmd => {
  return promisseExec(cmd);
};

exports.backExec = cmd => {
  return exec(cmd);
};
