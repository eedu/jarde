/* @format */
'use strict';

const bo = require('./bo');

exports.info = (req, res) => {
  const {script, input} = req.body;

  var resp = {
    status: 500,
    sysinfo: {},
  };

  bo.info()
    .then(output => {
      resp.status = 200;
      resp.sysinfo = output;
      res.json(resp);
    })
    .catch(err => {
      console.log(err);
      resp.status = 404;
      res.json(resp);
    });
};
