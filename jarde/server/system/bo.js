/* @format */
'use strict';

const system = require('../utils/system.js');

exports.info = () => {
  return new Promise((resolve, reject) => {
    system
      .exec('bash -c "getent passwd {1000..60000}" | cut -d: -f1')
      .then(result => {
        const {stdout} = result;
        const system = {
          users: stdout.split('\n').filter(e => e.length),
        };

        resolve(system);
      });
  });
};
