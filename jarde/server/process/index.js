/* @format */
'use strict';

const bo = require('./bo');

exports.exec = (req, res) => {
  const {command} = req.params;
  const {input} = req.body;

  var resp = {
    status: 500,
    output: {},
  };

  bo.exec(command, input)
    .then(output => {
      resp.status = 200;
      resp.output = output;
      res.json(resp);
    })
    .catch(err => {
      resp.status = 404;
      res.json(resp);
    });
};

exports.create = (req, res) => {
  var command = req.params.command;

  var resp = {
    status: 500,
    process: {},
  };

  if (command) {
    resp.status = 200;
    resp.process.id = 123;
    resp.process.command = command;
  } else {
    resp.status = 401;
    resp.process = null;
  }

  res.json(resp);
};
