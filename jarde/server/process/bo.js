/* @format */
'use strict';

const system = require('../utils/system.js');

exports.exec = (command, inputs) => {
  return new Promise((resolve, reject) => {
    switch (typeof inputs) {
      case 'object':
        var outputs = {};
        inputs.forEach((e, i) => {
          system.exec(`./server/bin/${command} ${e}`).then(result => {
            const {stdout} = result;
            outputs[e] = stdout;
            if (i === inputs.length - 1) {
              resolve(outputs);
            }
          });
        });
        break;
      case 'string':
        system.exec(`./server/bin/${command} ${inputs}`).then(result => {
          const {stdout} = result;
          resolve(stdout);
        });
        break;
      default:
        reject();
    }
  });
};
