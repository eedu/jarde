/* @format */
'use strict';

const session = require('express-session');
var MemoryStore = require('memorystore')(session);
const bodyParser = require('body-parser');

const whitelist = require('./routes/whitelist.js');

const session_middleware = (app, server) => {
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());

  app.use(
    session({
      cookie: {maxAge: 86400000},
      resave: false,
      saveUninitialized: true,
      store: new MemoryStore({
        checkPeriod: 86400000, // prune expired entries every 24h
      }),
      secret: 'jardesession',
    }),
  );

  app.use((req, res, next) => {
    const url = req.url;
    if (!req.session.loggedIn && !whitelist.checkUrl(url)) {
      console.log('saying no to url', url);
      res.json({
        status: 401,
        message: 'Must login first',
      });
    } else {
      req.server_instance = server;
      next();
    }
  });
};

module.exports = (app, server) => {
  session_middleware(app, server);
};
