/* @format */
'use strict';

const bo = require('./bo');

exports.get_session = (req, res) => {
  var resp = {
    status: 500,
    session: {},
  };

  if (req.session) {
    resp.status = 200;
    resp.session = req.session;
  } else {
    resp.session = null;
  }

  res.json(resp);
};

exports.login = (req, res) => {
  var timestamp;
  var resp = {
    status: 500,
    session: {},
  };
  const {username, password} = req.body;
  const {session} = req;

  bo.login(session, username, password)
    .then(session => {
      timestamp = new Date().getTime();
      resp.status = 200;
      resp.session = session;
      req.session = session;
      res.json(resp);
    })
    .catch(err => {
      console.log(err);
      resp.status = 401;
      resp.session = null;
      res.json(resp);
    });
};

exports.logout = (req, res) => {
  var ip = '';
  var timestamp = new Date().getTime();
  var session_id = req.body.session_id;
  var resp = {
    status: 500,
  };
  var session = Session.byId(session_id);

  if (session && session.is_running()) {
    session.finish();
    resp.status = 200;
  } else {
    resp.status = 404;
  }

  res.json(resp);
};
