/* @format */
'use strict';

const system = require('../utils/system');
const terminal = require('../terminal/bo');

function init_session(session, username) {
  return new Promise((resolve, reject) => {
    session.loggedIn = true;
    session.username = username;
    session.processes = {};
    system.exec(`getent passwd ${username} | cut -d: -f6`).then(result => {
      const res = result.stdout.trim();
      session.pwd = res;
      session.home = res;
      resolve(session);
    });
  });
}

exports.login = (session, username, password) => {
  return new Promise((resolve, reject) => {
    system
      .exec(`echo ${password} | su ${username} -c "echo success"`)
      .then(result => {
        const res = result.stdout.trim();
        if (res === 'success') {
          resolve();
        } else {
          reject('unexpected result');
        }
      })
      .catch(err => {
        reject('invalid authentication');
      });
  }).then(() => {
    return init_session(session, username);
  });
};
