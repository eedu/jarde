#!/bin/bash

SCREEN_NAME="dev"
WINDOW_0_NAME="Server"
WINDOW_1_NAME="Client"
WINDOW_2_NAME="Sass"
WINDOW_3_NAME="Menu"

script0="cd $PWD; "`which yarn`" start"
script1="cd $PWD/client; "`which yarn`" watch"
script2="cd $PWD/client/public/theme; "`which sass`" --watch sass/jade.scss:css/jade.css"

tmpfile0=$(mktemp)
chmod +x $tmpfile0
tmpfile1=$(mktemp)
chmod +x $tmpfile1
tmpfile2=$(mktemp)
chmod +x $tmpfile2
tmpfile3=$(mktemp)
chmod +x $tmpfile3
tmpfile4=$(mktemp)
chmod +x $tmpfile4

echo `which bash`" -c '$script0'">"$tmpfile0"
echo `which bash`" -c '$script1'">"$tmpfile1"
echo `which bash`" -c '$script2'">"$tmpfile2"

cat <<EOF > "$tmpfile3"
#!/bin/bash
op4="Quit"

script0_state=1
script1_state=1
script2_state=1

while true; do
    clear
    if [[ "\${script0_state}" == 1 ]]; then
        op1="Stop node server"
    else
        op1="Start node server"
    fi
    if [[ "\${script1_state}" == 1 ]]; then
        op2="Stop node client"
    else
        op2="Start node client"
    fi
    if [[ "\${script2_state}" == 1 ]]; then
        op3="Stop sass"
    else
        op3="Start sass"
    fi

    PS3='Please enter your choice: '
    options=("\$op1" "\$op2" "\$op3" "\$op4")
    select opt in "\${options[@]}"
    do
        case "\$opt" in
            "\$op1")
                if [[ "\${script0_state}" == 1 ]]; then
                    screen -SX $SCREEN_NAME -p 0 kill

                    script0_state=0
                else
                    screen -SX $SCREEN_NAME focus
                    screen -SX $SCREEN_NAME screen -t "$WINDOW_0_NAME" -s "$tmpfile0"
                    screen -SX $SCREEN_NAME focus
                    screen -SX $SCREEN_NAME focus
                    screen -SX $SCREEN_NAME focus

                    script0_state=1
                fi
                break;
                ;;
            "\$op2")
                if [[ "\${script1_state}" == 1 ]]; then
                    screen -SX $SCREEN_NAME -p 1 kill

                    script1_state=0
                else
                    screen -SX $SCREEN_NAME focus
                    screen -SX $SCREEN_NAME focus
                    screen -SX $SCREEN_NAME focus
                    screen -SX $SCREEN_NAME screen -t "$WINDOW_1_NAME" -s "$tmpfile1"
                    screen -SX $SCREEN_NAME focus

                    script1_state=1
                fi
                break;
                ;;
            "\$op3")
                if [[ "\${script2_state}" == 1 ]]; then
                    screen -SX $SCREEN_NAME -p 2 kill

                    script2_state=0
                else
                    screen -SX $SCREEN_NAME focus
                    screen -SX $SCREEN_NAME focus
                    screen -SX $SCREEN_NAME screen -t "$WINDOW_2_NAME" -s "$tmpfile2"
                    screen -SX $SCREEN_NAME focus
                    screen -SX $SCREEN_NAME focus

                    script2_state=1
                fi
                break;
                ;;
            "\$op4")
                rm "$tmpfile0"
                rm "$tmpfile1"
                rm "$tmpfile2"
                rm "$tmpfile3"
                rm "$tmpfile4"
                screen -SX $SCREEN_NAME quit
                break;
                ;;
            *) echo "invalid option \$REPLY";;
        esac
    done
done
EOF

cat <<EOF > "$tmpfile4"
screen -t "$WINDOW_0_NAME" "$tmpfile0"
split -v
focus
screen -t "$WINDOW_1_NAME" "$tmpfile1"

focus
split
focus
resize -10
screen -t "$WINDOW_2_NAME" "$tmpfile2"
focus
split 
focus
resize -10
screen -t "$WINDOW_3_NAME" "$tmpfile3"
EOF

killall node
screen -SX "$SCREEN_NAME" quit
screen -S "$SCREEN_NAME" -c "$tmpfile4"
